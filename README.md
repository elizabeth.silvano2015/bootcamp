## Conteúdo do Bootcamp

* Introdução ao Full Stack
* Principais recursos do Vue
* CSS Grid Layout
* Gerenciamento de estado com Vuex
* Atomic Design

# Projeto CRUD

## Rodando o Backend (servidor)

#### Vá para a pasta server
$ cd /Frontend-com-Vuejs/CRUD/backend

#### Instale as dependências
$ npm install

#### Execute a aplicação em modo de desenvolvimento
$ npm run dev

## Rodando a aplicação web (Frontend)

#### Vá para a pasta da aplicação Frontend
$ cd /Frontend-com-Vuejs/CRUD/frontend

#### Instale as dependências
$ npm install

#### Execute a aplicação em modo de desenvolvimento
$ npm run serve

#### Acesse http://localhost:8080/ 

# Projeto FAQ

#### Vá para a pasta do projeto
$ cd /Gerenciamento-Vuex/desafio

#### Instale as dependências
$ npm install

#### Execute a aplicação em modo de desenvolvimento
$ npm run dev

#### Acesse http://localhost:8080/ 

